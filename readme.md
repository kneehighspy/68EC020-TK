# 68EC020-TK
 
This is a 68EC020 CPU turbo-card for the 68000-CPU Slot of an Amiga 500, 1000 and 2000 (It doesn't fit into a CDTV :( ).
![Prototype rev 0.1](https://gitlab.com/MHeinrichs/68EC020-TK/raw/master/Pics/TK68EC020-K.jpg)

The CPU runs at 28MHz and can be either PGA or QFP-package. Beside the CPU it has 16MB of SD-RAM on board. Furthermore, it has a BSC-AT-Bus-compatible IDE controller on board and works with the Oktapussy driver found on the [aminet](http://aminet.net/package/disk/misc/oktapus "Online for ages!" ) (sorry for the name but that's what it's called).
Since the 68EC020 has only a 24-bit wide addressspace not the whole 16Mb can be used! 8MB are autoconfig, 1,5MB Ranger mem are available and 1MB in the ununsed Space betweed $A00000-$AFFFFF are visible. Furthermore an untested map-ROM feature is enabled for the memory between $F00000-$FFFFFF when writing to $FFFFFF 

**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!

## Theory of operation
* This PCB has four layers and consists of the CPU, a buffered IDE-Bus, an AutoBoot-ROM, a controlling Xilinx XC95288XL CPLD, two 8MB SD-RAM Chips running at 112MHz a PLL and various BUS-drivers and latches.
* The CPLD does various things:
    * It runs the bus translation betweed the 68000-host and the 68EC020 CPU
    * It generates the E-Clock and controlls the 6800-preipheral cycles
    * It does the DMA controll
    * It controlls the SD-RAM 
    * It autoconfigures the RAM for the Amiga in the address space $200000-$9FFFFF.
    * It maps 1.5MB of memory in the "ranger-mem" area between $C00000-$D7FFFF
    * It maps 1MB in the Workbench-ROM area between $A00000-$AFFFFF 
    * It autoconfigures the IDE
    * It sets the propper controll lines for the IDE-controller.


## What is in this repository?
This repository consists of several directories:
* Layout: The board, schematic and bill of material (BOM) are in this directory. There are two versions of the PCB: V02 for the PGA-Version and V03 for the QFP version of the 68EC020
* Logic: Here the code, project files and pin-assignments for the CPLD is stored
* Pics: Some pictures of this project
* root directory: the readme

## How to open the PCB-files?
You find the eagle board and schematic files (brd/sch) for the pcb. The software can be [downloaded](https://www.autodesk.com/products/eagle/overview) for free after registration. KiCad can import these files too. But importers are not 100% reliable... 

## How to make a PCB
You can generate Gerber-files from the brd files according to the specs of your PCB-manufactor. However, you can try to use the Gerber-files provided. Some specs: min trace width: 0.15mm/6mil, min trace dist: 0.15mm/6mil, min drill: 0.3mm

**ATTENTION: THE PCB has 4 layers!**

## How to get the parts?
Most parts can be found on digikey or mouser or a parts supplier of your choice. The CPU is the most difficult part to get. Be aware of ebay! A lot of fake CPUs are offered! A list of parts is found in the file [68EC020TK-V03.txt](https://gitlab.com/MHeinrichs/68EC020-TK/blob/master/Layout/68EC020TK-V03.txt)

## How to programm the board?
The CPLD must be programmed via Xilinx IMpact and an apropriate JTAG-dongle. The JED-File is provided. Additionally you have to programm an 29F010 or 29F040 and put it in the socket for autoboot. Otherwise the autoboot-function will not be enabled and you have to use the programm "AT" in the [oktapussy-driver package](http://aminet.net/package/disk/misc/oktapus) to mount your IDE-drives.

## It doesn't work! How to I get help?
For support visit the [a1k-forum](https://www.a1k.org/forum/showthread.php?p=1109207). Yes, you need to register and it's German, but writing in English is accepted. For bug-reports write a ticket here ;). 

